/* Copyright (c) 2015 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.omg.CosNaming.NamingContextPackage.NotFound;

/**
 * An implementation of Graph.
 * 
 * <p>PS2 instructions: you MUST use the provided rep.
 */
public class ConcreteEdgesGraph implements Graph<String> {
    
    private final Set<String> vertices = new HashSet<>();
    private final List<Edge> edges = new ArrayList<>();
    
    // Abstraction Function:
    //   represents the directed weighted graph
    //   which defined vertices and edges    
    // Rep invariant:
    //   edges are not duplicate    
    // Safety from rep exposure:
    //   All fields are private and elements of fields are immutable,
    //   methods isEdgeNotDuplicate() and vertices() use defensive copy.
    
    // Check that the rep invariant is true
    private void checkRep() {
        assert(this.isEdgeNotDuplicate()) : "Edges are duplicate";
        assert(this.isVerticesNotNull()) : "Some vertex is null";
    }
    
    /**
     * Check that edges are not duplicate
     * @return true if edges are not duplicate
     */
    private boolean isEdgeNotDuplicate() {
        List<Edge> sortedEdges = new ArrayList<>(edges);
        if (sortedEdges.size() > 1) {
        // sort edges by source and target
            sortedEdges.sort((a, b) -> a.getTarget().compareTo(b.getTarget()));
            sortedEdges.sort((a, b) -> a.getSource().compareTo(b.getSource()));
            String source = sortedEdges.get(0).getSource();
            String target = sortedEdges.get(0).getTarget();
            for (Edge edge : sortedEdges.subList(1, sortedEdges.size())) {
                if (source.equals(edge.getSource())
                    && target.equals(edge.getTarget())) {
                    // if adjacent edges in sorted List are equal
                    return false;
                }
                source = edge.getSource();
                target = edge.getTarget();
            }
        }
        return true;
    }
    
    /**
     * Check that vertices are not null
     * @return true if all vertices not null
     */
    private boolean isVerticesNotNull() {
        for (String vertex : vertices) {
            if (vertex == null) {return false;}            
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Graph contains " + vertices.size() + " vertices and "
                + edges.size() + " edges";
    }
    
    @Override
    public boolean add(String vertex) {
        boolean result = false;
        if (!vertices.contains(vertex)) {
            vertices.add(vertex);
            result = true;
        }
        checkRep();
        return result;
    }
    
    @Override
    public int set(String source, String target, int weight) {
        if (weight == 0) {
            try {
                return deleteEdge(source, target);
            }
            catch (NotFound ex) {
                return 0;
            }
            finally {
                checkRep();
            }
        }
        else {
            try {
                int result = deleteEdge(source, target);
                edges.add(new Edge(source, target, weight));
                return result;
            }
            catch (NotFound ex) {
                add(source);
                add(target);
                edges.add(new Edge(source, target, weight));
                return 0;
            }
            finally {
                checkRep();
            }
        }
    }
    
    /**
     * Search the edge in the List of edges
     * @param source source vertex of the edge
     * @param target target vertex of the edge
     * @return edge if it's in the edges
     * @throws NotFound if edge isn't in the edges
     */
    private Edge find(String source, String target) throws NotFound  {
        for (Edge edge : edges) {
            if (edge.getSource().equals(source)
                && edge.getTarget().equals(target)) {
                return edge;
            }
        }
        throw new NotFound();
    }
    
    /**
     * Delete edge from List of edges
     * @param source source vertex of the edge
     * @param target target vertex of the edge
     * @return weight of deleted edge
     * @throws NotFound if edge is not in List of edges
     */
    private int deleteEdge(String source, String target) throws NotFound {
        Edge edge = find(source, target);
        int result = edge.getWeight();
        edges.remove(edge);
        return result;
    }
    
    @Override
    public boolean remove(String vertex) {
        boolean result = false;
        if (vertices.contains(vertex)) {
            // delete vertex
            vertices.remove(vertex);
            // collect edges with source in vertex
            List<Edge> edgesForRemove = findEdgesBySource(vertex);
            // and edges with target in vertex
            edgesForRemove.addAll(findEdgesByTarget(vertex));
            // then delete found edges
            edges.removeAll(edgesForRemove);
            result = true;
        }
        checkRep();
        return result;
    }
    
    /**
     * Search edges with known source in list of edges
     * @param source the source of edges
     * @return list of found edges
     */
    private List<Edge> findEdgesBySource(String source) {
        List<Edge> result = new ArrayList<>();
        for (Edge edge : edges) {
            if (edge.getSource().equals(source)) {
                result.add(edge);
            }
        }
        return result;
    }
    
    /**
     * Search edges with known target in list of edges
     * @param target the target of edges
     * @return list of found edges
     */
    private List<Edge> findEdgesByTarget(String target) {
        List<Edge> result = new ArrayList<>();
        for (Edge edge : edges) {
            if (edge.getTarget().equals(target)) {
                result.add(edge);
            }
        }
        return result;
    }
    
    @Override
    public Set<String> vertices() {
        checkRep();
        return new HashSet<>(vertices);
    }
    
    
    @Override
    public Map<String, Integer> sources(String target) {
        Map<String, Integer> result = new HashMap<>();
        List<Edge> findingEdges = findEdgesByTarget(target);
        for (Edge edge : findingEdges) {
            result.put(edge.getSource(), edge.getWeight());
        }
        return result;
    }
    
    @Override
    public Map<String, Integer> targets(String source) {
        Map<String, Integer> result = new HashMap<>();
        List<Edge> findingEdges = findEdgesBySource(source);
        for (Edge edge : findingEdges) {
            result.put(edge.getTarget(), edge.getWeight());
        }
        return result;
    }    
    
    /**
     * Specification:
     * Immutable.
     * This class is internal to the rep of ConcreteEdgesGraph.
     * Each edge has a source vertex and a target vertex.
     * Both vertices must have the same immutable type.
     * Edges are directed (point from source to target).
     * Each edge has a positive weight of type int.
     * The vertices of an edge don't necessarily have to exist in the graph
     *  (we could just be checking to see if the graph contains this edge).
     *  
     *  @param source : the vertex from which the edge originates
     *  @param target : the vertex to which the edge points
     *  @param weight : the positive int value representing the cost/weight of the edge
     * 
     * <p>PS2 instructions: the specification and implementation of this class is
     * up to you.
     */
    static class Edge {    
        
        private String source;
        private String target;
        private int weight;
        
        // Abstraction Function:
        //   represents the weighted directed edge from source to target
        //   and has value equal weight
        // Rep invariant:
        //   weight is positive
        // Safety from rep exposure:
        //   All fields are private and all types in the rep are immutable
        
        // Check that the rep invariant is true
        private void checkRep() {
            assert (weight > 0) : "Invalid value of weight";
            assert (source != null) : "sourse not exists";
            assert (target != null) : "target not exists";
        }
        
        @Override
        public String toString() {
            return "Sourse = " + source + " Target = " + target + " Weight = " + weight;
        }
        
        /**
         * Create a new edge object
         * @param source vertex which is source of edge
         * @param target vertex which is target of edge
         * @param weight weight of the edge, must be positive
         */
        public Edge(String source, String target, int weight) {
            this.source = source;
            this.target = target;
            this.weight = weight;
            checkRep();
        }
        
        /**
         * Get source of the edge
         * @return vertex which is a source of the edge
         */
        public String getSource() {
            checkRep();
            return source;
        }
        
        /**
         * Get target of the edge
         * @return vertex which is a target of the edge
         */
        public String getTarget() {
            checkRep();
            return target;
        }
        
        /**
         * Get weight of the edge
         * @return weight of the edge
         */
        public int getWeight() {
            checkRep();
            return weight;
        }
        
        /**
         * Compare two edges
         * @param other second edge
         * @return true if edges are equal
         */
        public boolean isSame(Edge other) {
            boolean result = false;
            if (source.equals(other.source)
                && target.equals(other.target)) {
                
                result =  true;
            }
            checkRep();
            return result;
        }
    }
}