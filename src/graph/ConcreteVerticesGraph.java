/* Copyright (c) 2015 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * An implementation of Graph.
 * 
 * <p>PS2 instructions: you MUST use the provided rep.
 */
public class ConcreteVerticesGraph implements Graph<String> {
    
    private final List<Vertex> vertices = new ArrayList<>();
    
    // Abstraction function:
    //   represents graph as set of Vertex objects
    // Rep invariant:
    //   all vertices are distinct    
    // Safety from rep exposure:
    //   field is private and elements of fields are immutable,
    //   methods isEdgeNotDuplicate() and vertices() use defensive copy.
    
    
    
    @Override
    public boolean add(String vertex) {
        throw new RuntimeException("not implemented");
    }
    
    @Override
    public int set(String source, String target, int weight) {
        throw new RuntimeException("not implemented");
    }
    
    @Override
    public boolean remove(String vertex) {
        throw new RuntimeException("not implemented");
    }
    
    @Override
    public Set<String> vertices() {
        throw new RuntimeException("not implemented");
    }
    
    @Override
    public Map<String, Integer> sources(String target) {
        throw new RuntimeException("not implemented");
    }
    
    @Override
    public Map<String, Integer> targets(String source) {
        throw new RuntimeException("not implemented");
    }    
    
    /**
     * Specification
     * Mutable.
     * This class is internal to the rep of ConcreteVerticesGraph.
     * A vertex must have:
     *      -a label of type String.
     *      -a list of outgoing edges (the vertex must be the source)
     *      -a list of incoming edges (the vertex must be the target)
     * There can be no duplicate vertices in a graph.
     * the list of outgoing edges is mutable and private
     * the list of incoming edges is mutable and private
     * A vertex can have an edge going to itself
     * 
     * <p>PS2 instructions: the specification and implementation of this class is
     * up to you.
     */    
    static class Vertex {
        
        private String label;
        private final Map<String, Integer> incomingEdges = new HashMap<>();
        private final Map<String, Integer> outgoingEdges = new HashMap<>();
        
        // Abstraction function:
        //   represents vertex of weighted graph labeled as label with
        //   sets of incoming edges incomingEdges and outgoing edges outgoingEdges
        // Rep invariant:
        //   Values of Maps(incomingEdges and outoingEdges) are positive    
        // Safety from rep exposure:
        //   All fields are private, label is immutable, elements of incomingEdges and
        //   outgoingEdges are immutable.
        //   methods isEdgeNotDuplicate() and vertices() use defensive copy.
        
        // Check that the rep invariant is true
        private void checkRep() {
            assert(isPositive(incomingEdges)) : "Some incoming edges aren't positive";
            assert(isPositive(incomingEdges)) : "Some outgoing edges aren't positive";
            assert(label != null) : "label is null";
        }
        
        /**
         * Check value of edge's weight
         * @param edges Map of edges
         * @return yes if weight is positive
         */
        private boolean isPositive(Map<String, Integer> edges) {
            for (String vertex : edges.keySet()) {
                if (edges.get(vertex) < 1) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public String toString() {
            return "Vertex " + label + " contains " + incomingEdges.size() + " incoming edges and "
                    + outgoingEdges.size() + " outgoing edges";
        }
        
        /**
         * Create disconnected vertex
         * @param label label of vertex
         */
        public Vertex(String label) {
            throw new RuntimeException("not implemented");
        }
        
        /**
         * Add or change incoming edge
         * @param label label of source vertex
         * @param weight weight of the edge
         */
        public void changeIncomingEdge(String label, int weight) {
            throw new RuntimeException("not implemented");
        }
        
        /**
         * Add or change outgoing edge
         * @param label label of target vertex
         * @param weight weight of the edge
         */
        public void changeOutgoingEdge(String label, int weight) {
            throw new RuntimeException("not implemented");
        }
        
        /**
         * Delete incoming edge
         * @param label label of source vertex
         */
        public void deleteIncomingEdge(String label) {
            throw new RuntimeException("not implemented");
        }
        
        /**
         * Delete outgoing edge
         * @param label label of target vertex
         */
        public void deleteOutgoingEdge(String label) {
            throw new RuntimeException("not implemented");
        }
    }
}


