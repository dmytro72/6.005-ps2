/* Copyright (c) 2015 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

/**
 * Tests for instance methods of Graph.
 * 
 * <p>PS2 instructions: you MUST NOT add a constructor to this class or change
 * the spec of {@link #emptyInstance()}.
 * Your tests MUST only obtain Graph instances by calling emptyInstance().
 */
public abstract class GraphInstanceTest {
    
    /*
     * Testing strategy
     * 
     * add()
     *  graph consist this vertex : yes, no
     *  initial graph size : 0, 1, >1
     * remove()
     *  graph consist this vertex : yes, no
     *  vertex has edges : outgoing, incoming
     *  initial graph size : 0, 1, >1
     * set()
     *  source equal target : yes, no
     *  source is in graph : yes, no
     *  target is in graph : yes, no
     *  weight : 0, 1, >1
     *  source and target was connected : yes, no
     *  initial graph size : 0, 1, >1
     * source()
     *  target vertex is in graph : yes, no
     *  source vertexes number : 0, 1, >1
     *  initial graph size : 0, 1, >1
     * target()
     *  source vertex is in graph : yes, no
     *  target vertexes number : 0, 1, >1
     *  graph size : 0, 1, >1
     * vertices()
     *  graph size : 0, 1, >1
     */
    private static final String vertex1 = "V1";
    private static final String vertex2 = "V2";
    private static final String vertex3 = "V3";
    private static final String vertex4 = "V4";
    
    private static final int weight0 = 0;
    private static final int weight1 = 1;
    private static final int weight2 = 2;
    
    /**
     * Overridden by implementation-specific test classes.
     * 
     * @return a new empty graph of the particular implementation being tested
     */
    public abstract Graph<String> emptyInstance();
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    // test add()
    // cover graph added new vertex
    //       initial graph size = 0
    @Test
    public void testAddNewVertexToEmptyGraph() {
        Graph<String> testGraph = emptyInstance();
        boolean isAdded = testGraph.add(vertex1);
        assertTrue(testGraph.vertices().contains(vertex1));
        assertTrue(isAdded);
    }

    // test add()
    // cover graph added new vertex
    //       initial graph size = 2
    @Test
    public void testAddNewVertexToNonemptyGraph() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex1);
        testGraph.add(vertex2);
        boolean isAdded = testGraph.add(vertex3);
        assertTrue(testGraph.vertices().contains(vertex3));
        assertTrue(isAdded);
    }

    // test add()
    // cover graph added contained vertex
    //       initial graph size = 1
    @Test
    public void testAddContainedVertexToGraph() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex1);
        Set<String> verticesBefore = testGraph.vertices();
        boolean isAdded = testGraph.add(vertex1);
        assertTrue(verticesBefore.containsAll(testGraph.vertices()));
        assertEquals(verticesBefore.size(), testGraph.vertices().size());
        assertFalse(isAdded);
    }
    
    // test remove()
    // cover graph removed contained vertex
    //       vertex contain incoming edge
    //       initial graph size = 2
    @Test
    public void testRemoveContainedVertexWithIncomingEdgeFromGraph() {
        Graph<String> testGraph = emptyInstance();
        testGraph.set(vertex2, vertex1, weight1);
        Map<String, Integer> edges = testGraph.sources(vertex1);
        boolean isRemoved = testGraph.remove(vertex1);
        for (String vertex : edges.keySet()) {
            assertFalse(testGraph.targets(vertex).containsKey(vertex1));
        }
        assertFalse(testGraph.vertices().contains(vertex1));
        assertTrue(isRemoved);
    }
    
    // test remove()
    // cover graph removed contained vertex
    //       vertex contain outgoing edge
    //       initial graph size = 2
    @Test
    public void testRemoveContainedVertexWithOutgoingEdgeFromGraph() {
        Graph<String> testGraph = emptyInstance();
        testGraph.set(vertex1, vertex2, weight1);
        Map<String, Integer> edges = testGraph.targets(vertex1);
        boolean isRemoved = testGraph.remove(vertex1);
        for (String vertex : edges.keySet()) {
            assertFalse(testGraph.sources(vertex).containsKey(vertex1));
        }
        assertFalse(testGraph.vertices().contains(vertex1));
        assertTrue(isRemoved);
    }
    
    // test remove()
    // cover graph removed contained vertex
    //       initial graph size = 0
    @Test
    public void testRemoveNewVertexFromEmptyGraph() {
        Graph<String> testGraph = emptyInstance();
        Set<String> verticesBefore = testGraph.vertices();
        boolean isRemoved = testGraph.remove(vertex1);
        assertTrue(verticesBefore.containsAll(testGraph.vertices()));
        assertEquals(verticesBefore.size(), testGraph.vertices().size());
        assertFalse(isRemoved);
    }
    
    // test set()
    // cover graph not consist this vertexes
    //       source not equal target
    //       weight = 1
    //       source and target was not connected
    //       initial graph size = 0
    @Test
    public void testSetEdgeBetweenNewVertexes() {
        Graph<String> testGraph = emptyInstance();
        int previousWeight = testGraph.set(vertex1, vertex2, weight1);
        assertEquals(0, previousWeight);
        assertTrue(testGraph.targets(vertex1).get(vertex2) == weight1);
        assertEquals(2, testGraph.vertices().size());        
    } 
    
    // test set()
    // cover graph not consist source
    //       graph consist target
    //       source not equal target
    //       weight = 2
    //       source and target was not connected
    //       initial graph size = 1
    @Test
    public void testSetEdgeWithNewSource() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex2);
        int previousWeight = testGraph.set(vertex1, vertex2, weight2);
        assertEquals(0, previousWeight);
        assertTrue(testGraph.targets(vertex1).get(vertex2) == weight2);
        assertEquals(2, testGraph.vertices().size());   
    }
    
    
    // test set()
    // cover graph consist source
    //       graph not consist target
    //       source not equal target
    //       weight = 0
    //       source and target was not connected
    //       initial graph size = 1
    @Test
    public void testSetEdgeWithNewTargetWeight0() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex1);
        int previousWeight = testGraph.set(vertex1, vertex2, weight0);
        assertEquals(0, previousWeight);
        assertFalse(testGraph.vertices().contains(vertex2));
        assertEquals(1, testGraph.vertices().size()); 
    }
    
    // test set()
    // cover graph consist this vertexes
    //       source equal target
    //       weight = 1
    //       source and target was not connected
    //       initial graph size = 1
    @Test
    public void testSetEdgeSourceEqualTarget() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex1);
        int previousWeight = testGraph.set(vertex1, vertex1, weight1);
        assertEquals(0, previousWeight);
        assertTrue(testGraph.targets(vertex1).get(vertex1) == weight1);
        assertEquals(1, testGraph.vertices().size()); 
    }
    
    // test set()
    // cover graph consist source
    //       graph consist target
    //       source not equal target
    //       weight = 2
    //       source and target was connected
    //       initial graph size = 2
    @Test
    public void testSetEdgeModifier() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex1);
        testGraph.add(vertex2);
        testGraph.set(vertex1, vertex2, weight1);
        int previousWeight = testGraph.set(vertex1, vertex2, weight2);
        assertEquals(weight1, previousWeight);
        assertTrue(testGraph.targets(vertex1).get(vertex2) == weight2);
        assertEquals(2, testGraph.vertices().size()); 
    }
    
    // test set()
    // cover graph consist source
    //       graph consist target
    //       source not equal target
    //       weight = 0
    //       source and target was connected
    //       initial graph size = 2
    @Test
    public void testSetEdgeDelete() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex1);
        testGraph.add(vertex2);
        testGraph.set(vertex1, vertex2, weight1);
        int previousWeight = testGraph.set(vertex1, vertex2, weight0);
        assertEquals(weight1, previousWeight);
        assertFalse(testGraph.targets(vertex1).containsKey(vertex2));
        assertEquals(2, testGraph.vertices().size());        
    }
    
    // test source()
    // cover vertex is not in graph
    //       source vertex number = 0
    //       initial graph size = 0
    @Test
    public void testSoucesAtEmptyGraph() {
        Graph<String> testGraph = emptyInstance();
        Map<String, Integer> sources = testGraph.sources(vertex1);
        assertEquals(Collections.emptyMap(), sources);     
    }
    
    // test source()
    // cover vertex is in graph
    //       source vertex number = 1
    //       initial graph size = 1
    @Test
    public void testSourcesWithOneSource() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex1);
        testGraph.set(vertex1, vertex1, weight1);
        Map<String, Integer> sources = testGraph.sources(vertex1);
        assertEquals(1, sources.size());
        assertTrue(sources.get(vertex1) == weight1);       
    }
    
    // test source()
    // cover vertex is in graph
    //       source vertex number = 3
    //       initial graph size = 4
    @Test
    public void testSourcesWithSomeSource() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex1);
        testGraph.add(vertex2);
        testGraph.add(vertex3);
        testGraph.add(vertex4);
        testGraph.set(vertex2, vertex1, weight1);
        testGraph.set(vertex3, vertex1, weight2);
        testGraph.set(vertex4, vertex1, weight1);
        Map<String, Integer> sources = testGraph.sources(vertex1);
        assertEquals(3, sources.size());
        assertTrue(sources.get(vertex2) == weight1);
        assertTrue(sources.get(vertex3) == weight2);
        assertTrue(sources.get(vertex4) == weight1);
    }
    
    // test target()
    // cover vertex is not in graph
    //       target vertex number = 0
    //       initial graph size = 0// test source()
    @Test
    public void testTargetsAtEmptyGraph() {
        Graph<String> testGraph = emptyInstance();
        Map<String, Integer> targets = testGraph.targets(vertex1);
        assertEquals(Collections.emptyMap(), targets); 
    }
    
    // test target()
    // cover vertex is in graph
    //       target vertex number = 1
    //       initial graph size = 1
    @Test
    public void testTargetsWithOneTarget() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex1);
        testGraph.set(vertex1, vertex1, weight1);
        Map<String, Integer> targets = testGraph.targets(vertex1);
        assertEquals(1, targets.size());
        assertTrue(targets.get(vertex1) == weight1);
    }
    
    // test target()
    // cover vertex is in graph
    //       target vertex number = 3
    //       initial graph size = 4
    @Test
    public void testTargetsWithSomeTargets() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex1);
        testGraph.add(vertex2);
        testGraph.add(vertex3);
        testGraph.add(vertex4);
        testGraph.set(vertex1, vertex2, weight1);
        testGraph.set(vertex1, vertex3, weight2);
        testGraph.set(vertex1, vertex4, weight1);
        Map<String, Integer> targets = testGraph.targets(vertex1);
        assertEquals(3, targets.size());
        assertTrue(targets.get(vertex2) == weight1);
        assertTrue(targets.get(vertex3) == weight2);
        assertTrue(targets.get(vertex4) == weight1);
    }
    
    // test vertices()
    // cover initial graph size = 0    
    @Test
    public void testInitialVerticesEmpty() {
        // you may use, change, or remove this test
        assertEquals("expected new graph to have no vertices",
                Collections.emptySet(), emptyInstance().vertices());
    }
    
    // test vertices()
    // cover initial graph size = 1
    @Test
    public void testVerticesOneElementGraph() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex1);
        Set<String> vertises = testGraph.vertices();
        assertEquals(1, vertises.size());
        assertTrue(vertises.contains(vertex1));
    }
    
    // test vertices()
    // cover initial graph size = 3
    @Test
    public void testVerticesMultipleElementsGraph() {
        Graph<String> testGraph = emptyInstance();
        testGraph.add(vertex1);
        testGraph.add(vertex2);
        testGraph.add(vertex3);
        Set<String> vertises = testGraph.vertices();
        
        Set<String> modelVertises = new HashSet<>(Arrays.asList(vertex1, vertex2, vertex3));
        
        assertTrue(vertises.containsAll(modelVertises));
        assertEquals(3, vertises.size());
    }
}
