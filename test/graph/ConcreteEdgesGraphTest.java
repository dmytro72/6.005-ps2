/* Copyright (c) 2015 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import graph.ConcreteEdgesGraph.Edge;

/**
 * Tests for ConcreteEdgesGraph.
 * 
 * This class runs the GraphInstanceTest tests against ConcreteEdgesGraph, as
 * well as tests for that particular implementation.
 * 
 * Tests against the Graph spec should be in GraphInstanceTest.
 */
public class ConcreteEdgesGraphTest extends GraphInstanceTest {
    
    /*
     * Testing strategy for Edge class
     * 
     * Edge()
     *   source equal target : yes, no
     *   weight value : 1, > 1
     * getSource()
     *   source symbols : 0, 1, >1
     * getTarget()
     *   target symbols : 0, 1, >1
     * getWeight()
     *   weight value : 1, >1
     * isSame()
     *   targets equal : yes, no
     *   sources equal : yes, no
     * toString()
     *   source equal target : yes, no
     *   weight value : 1, >1
     */
    
    /*
     * Testing strategy for ConcreteEdgesGraph class
     * toString()
     *   vertices : 0, 1, >1
     *   edges : 0, 1, >1
     */
    
    private final String vertex1 = "V1";
    private final String vertex2 = "V";
    private final String vertex3 = "";
    private final int weight1 = 1;
    private final int weight2 = 2;
    
    /*
     * Provide a ConcreteEdgesGraph for tests in GraphInstanceTest.
     */
    @Override public Graph<String> emptyInstance() {
        return new ConcreteEdgesGraph();
    }
    
    // Testing Edge...
    
    // cover source equal target
    //       source.length = 2
    //       target.length = 2
    //       weight = 1
    @Test
    public void testEdgeEqualVertises() {
        Edge edge = new Edge(vertex1, vertex1, weight1);
        assertEquals(vertex1, edge.getSource());
        assertEquals(vertex1, edge.getTarget());
        assertEquals(weight1, edge.getWeight());
    }
    
    // cover source not equal target
    //       source.length = 2
    //       target.length = 1
    //       weight = 2
    @Test
    public void testEdgeDifferentVertises() {
        Edge edge = new Edge(vertex1, vertex2, weight2);
        assertEquals(vertex1, edge.getSource());
        assertEquals(vertex2, edge.getTarget());
        assertEquals(weight2, edge.getWeight());
    }
    
    // cover source not equal target
    //       source.length = 1
    //       target.length = 0
    //       weight = 1
    @Test
    public void testEdgeSourseOneSymbol() {
        Edge edge = new Edge(vertex2, vertex3, weight1);
        assertEquals(vertex2, edge.getSource());
        assertEquals(vertex3, edge.getTarget());
        assertEquals(weight1, edge.getWeight());
    }
    
    // cover source not equal target
    //       source.length = 0
    //       target.length = 1
    //       weight = 2
    @Test
    public void testEdgeEmptySourse() {
        Edge edge = new Edge(vertex3, vertex2, weight2);
        assertEquals(vertex3, edge.getSource());
        assertEquals(vertex2, edge.getTarget());
        assertEquals(weight2, edge.getWeight());
    }
    
    // cover targets equal
    //       sources equal
    @Test
    public void testEdgeSourcesAndTargetsEqual() {
        Edge edgeThis = new Edge(vertex1, vertex2, weight1);
        Edge edgeOther = new Edge(vertex1, vertex2, weight2);
        assertTrue(edgeThis.isSame(edgeOther));
    }
    
    // cover targets equal
    //       sources different
    @Test
    public void testEdgeTargetsDifferent() {
        Edge edgeThis = new Edge(vertex1, vertex2, weight2);
        Edge edgeOther = new Edge(vertex2, vertex2, weight2);
        assertFalse(edgeThis.isSame(edgeOther));
    }
    
    // cover targets different
    //       sources equal
    @Test
    public void testEdgeSourcesDifferent() {
        Edge edgeThis = new Edge(vertex1, vertex2, weight2);
        Edge edgeOther = new Edge(vertex1, vertex3, weight1);
        assertFalse(edgeThis.isSame(edgeOther));
    }
    
    // cover targets different
    //       sources different
    @Test
    public void testEdgeSourcesAndTargetsDifferent() {
        Edge edgeThis = new Edge(vertex1, vertex2, weight1);
        Edge edgeOther = new Edge(vertex2, vertex1, weight1);
        assertFalse(edgeThis.isSame(edgeOther));
    }
    
    // cover target equal source
    //       weight = 1
    @Test
    public void testToStringSourceEqualTarget() {
        Edge edge = new Edge(vertex1, vertex1, weight1);
        String modelString = "Sourse = V1 Target = V1 Weight = 1";
        assertEquals(modelString, edge.toString());
    }
    
    // cover target and source are different
    //       weight = 2
    @Test
    public void testToStringSourceAndTargetDifferent() {
        Edge edge = new Edge(vertex1, vertex2, weight2);
        String modelString = "Sourse = V1 Target = V Weight = 2";
        assertEquals(modelString, edge.toString());
    }
    
    // Testing ConcreteEdgesGraph...
    
    // cover number of vertices = 0
    //       number of edges = 0
    @Test
    public void testToStringEmptyGraph() {
        ConcreteEdgesGraph graph = new ConcreteEdgesGraph();
        String modelString = "Graph contains 0 vertices and 0 edges";
        assertEquals(modelString, graph.toString());
    }
    
    // cover number of vertices = 1
    //       number of edges = 1
    @Test
    public void testToStringOneVertexOneEdge() {
        ConcreteEdgesGraph graph = new ConcreteEdgesGraph();
        graph.set(vertex1, vertex1, weight1);
        String modelString = "Graph contains 1 vertices and 1 edges";
        assertEquals(modelString, graph.toString());
    }
    
    // cover number of vertices = 3
    //       number of edges = 2
    @Test
    public void testToStringThreeVertexTwoEdge() {
        ConcreteEdgesGraph graph = new ConcreteEdgesGraph();
        graph.set(vertex1, vertex2, weight1);
        graph.set(vertex2, vertex3, weight2);
        String modelString = "Graph contains 3 vertices and 2 edges";
        assertEquals(modelString, graph.toString());
    }
    
    
    
}
